# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pytest
pkgver=8.0.1
pkgrel=0
pkgdesc="Python3 testing library"
url="https://docs.pytest.org/en/latest/"
arch="noarch"
license="MIT"
depends="
	py3-iniconfig
	py3-packaging
	py3-pluggy
	py3-py
	python3
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="bash py3-hypothesis py3-virtualenv py3-xmlschema"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/pytest/pytest-$pkgver.tar.gz
	"
builddir="$srcdir/pytest-$pkgver"
options="!check" # causes bootstrapping issues because of checkdepends

replaces="pytest" # Backwards compatibility
provides="pytest=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare

	sed -e "/^\[metadata\]/a version = $pkgver" -i setup.cfg
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer .dist/pytest*.whl

	test-env/bin/python3 -m pytest
}

package() {
	mkdir -p "$pkgdir"/usr/bin

	local name; for name in py.test pytest; do
		ln -s $name-3 "$pkgdir"/usr/bin/$name
	done

	python3 -m installer -d "$pkgdir" \
		.dist/pytest*.whl
}

sha512sums="
ce1fa9fa2a78f73320e1692681930c7bee5f6bd66b592d6f8b7e6c958cbdc180d9b1f39ecad2556c0811c5337e7fa28b84a093f064ac46f7fd3c4a6d94b3e73a  pytest-8.0.1.tar.gz
"
