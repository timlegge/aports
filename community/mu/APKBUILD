# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: stef <l0ls0fo2i@ctrlc.hu>
# Contributor: Eivind Uggedal <eu@eju.no>
# Contributor: Nico Schottelius <nico.schottelius@ungleich.ch>
# Contributor: Justin Berthault <justin.berthault@zaclys.net>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mu
pkgver=1.12.0
pkgrel=0
pkgdesc="A collection of utilities for indexing and searching Maildirs"
url="https://www.djcbsoftware.nl/code/mu/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	emacs-nox
	glib-dev
	gmime-dev
	meson
	readline-dev
	texinfo
	xapian-core-dev
	"
subpackages="$pkgname-doc mu4e::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/djcb/mu/archive/v$pkgver.tar.gz
	mu-utils-stdout.patch
	"

prepare() {
	default_prepare

	abuild-meson \
		-Dguile=disabled \
		-Dreadline=enabled \
		build
}

build() {
	ninja -C build
}

check() {
	meson test -C build
}

package() {
	DESTDIR=$pkgdir meson install -C build
}

mu4e() {
	pkgdesc="Emacs-based e-mail client which uses mu as its back-end"
	depends="$pkgname=$pkgver-r$pkgrel cmd:emacs"

	amove usr/share
}

sha512sums="
4252f2305ad671375ec251dd111b56798ba675b289a99f0ea13a6b2a96a0562ed24bc26fd83025492a2d048440ab735650e18f3bec213af7b85dc7f9257033a6  mu-1.12.0.tar.gz
d3808f98b93954222731d373d96dc92f798fe78d90e6ed17a86aef7d4d010d414711bd231047e4147540f2907f016b66f6a829e0cd3126aa377aa88c669104d7  mu-utils-stdout.patch
"
